// выпадающий список

const btnWrapper = document.getElementById('furniture');

btnWrapper.addEventListener('click', function () {
    document.querySelector('.vector').classList.toggle('vector--active');
    document.querySelector('.list-item_menu').classList.toggle('none');
})

// mobile list

const mobBtn = document.querySelector('.menu-item');

mobBtn.addEventListener('click', function() {
    document.querySelector('.vector2').classList.toggle('vector--active');
    document.querySelector('.menu-mobile_list').classList.toggle('none');
})

// header btns

const buttons = document.querySelectorAll('.touch');
const touchChoises = document.querySelectorAll('.touch-choise');

for (let button of buttons) {
    button.addEventListener('click', showTouch) 
    }

function showTouch(e) {
    e.stopPropagation();
    this.parentNode.querySelector('.touch-choise').classList.toggle('none');  
}

document.addEventListener('click', closeTouchChoise)

function closeTouchChoise() {
    for (let touchChoise of touchChoises) {
        touchChoise.classList.add('none');
        }
}

for (let touchChoise of touchChoises) {
        touchChoise.addEventListener('click', (e) => e.stopPropagation());
    }

// tabs

const tabs = document.querySelectorAll('[data-tab]');
const cards = document.querySelectorAll('[data-tab-value]');


for(let tab of tabs) {
    tab.addEventListener('click', function () {

    // hidden active class

    for(let tab of tabs) {

    tab.classList.remove('active');
}

    // add active class
    
        this.classList.add('active');


    // hidden and show needs cards

    for (let card of cards) {

        if (this.dataset.tab === 'All') {
            card.classList.remove('none')
        } else if (card.dataset.tabValue == this.dataset.tab) {
            card.classList.remove('none')
        } else {
            card.classList.add('none')
        }
    }

    swiper.update()
    
    })
}

//swiper slider

const swiper = new Swiper('.swiper', {
    
    loop: true,
    freeMode: true,
    slidesPerView: 4,
    spaceBetween: 42,

    breakpoints: {
        320: {
            slidesPerView: 1,
            spaceBetween: 20,
        },

        560: {
            slidesPerView: 2,
            spaceBetween: 20,
        },
        830: {
            slidesPerView: 3,
            spaceBetween: 40,
        },
        1350: {
            slidesPerView: 4,
            spaceBetween: 40,
        },
    },

    // Navigation arrows
    navigation: {
        nextEl: '.swiper-button_next',
        prevEl: '.swiper-button_prev',
    },
});


